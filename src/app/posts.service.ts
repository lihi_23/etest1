import { CommentsService } from './comments.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';





@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL = "http://jsonplaceholder.typicode.com/posts";
  
  

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.URL); 
   
  }
  

  postCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  // addPost(userId,id:string,title:string,body:string){
  //   const post = {id:id,title:title,body:body}; 
  //   this.userCollection.doc(id).collection('posts').add(post);
  // }

  postsCollection:AngularFirestoreCollection;

  addPost(userId:string,id:string,title:string,body:string){
    const post = {id:id,title:title,body:body};
    console.log(userId)
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  public getMyPosts(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }
  public deletePost(userId:string , id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }
  updateLike(userId:string,id:string,Like:number){
         this.db.doc(`users/${userId}/posts/${id}`).update(
           {
              Like:Like
            }
         )
        }
  
  constructor(private http: HttpClient, private db:AngularFirestore) { }
}
