import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  email:string;
  users$;

  constructor(public AuthService:AuthService) { }

  ngOnInit(): void {
    this.users$ = this.AuthService.getUser();
    this.users$.subscribe(
      data => {
        this.email = data.email;
      }
    )
  }

}
