import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() id:string; 
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
  @Input() formType:string;
  isError:boolean = false;

  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, education:this.education, income:this.income};
    if (this.education > 24 || this.education <0){
      this.isError = true;
    }else{
      this.update.emit(customer); 
      if(this.formType == "Add customer"){
        this.name  = null;
        this.education = null; 
        this.income = null;
      }

    }

  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }



  constructor() { }

  ngOnInit(): void {
  }

}
