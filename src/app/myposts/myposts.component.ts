import { PostsService } from './../posts.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-myposts',
  templateUrl: './myposts.component.html',
  styleUrls: ['./myposts.component.css']
})
export class MypostsComponent implements OnInit {
  userId:string;
  posts$;

  public deletePost(id:string){
    this.postsService.deletePost(this.userId,id);
  }

  numLikes(id:string,Like:number){
       if(isNaN(Like)){
         Like = 1;
        } else{
          Like = Like + 1;
        }
        this.postsService.updateLike(this.userId,id,Like);
      }
  constructor(public authService:AuthService, private postsService:PostsService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user =>{
        this.userId = user.uid;
        console.log(this.userId);
        this.posts$ = this.postsService.getMyPosts(this.userId);


      }
    )

  }

}
