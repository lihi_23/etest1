import { PredictionService } from './../prediction.service';
import { Customer } from './../interfaces/customer';
import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';




@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers;
  customers$;
  userId:string;
  panelOpenState = false;
  editstate = [];
  addCustomerFormOPen = false;
  
  
  

  public deleteCustomer(id:string){
    this.CustomersService.deleteCustomer(this.userId,id);
  }

  update(customer:Customer){
    this.CustomersService.updateCustomer(this.userId,customer.id ,customer.name, customer.education,customer.income);
  }


  add(customer:Customer){
    this.CustomersService.addCustomer(this.userId,customer.name,customer.education,customer.income,customer.result = "",customer.st = 0); 
  }

  save(customer:Customer){
    this.CustomersService.save(this.userId,customer.id ,customer.name, customer.education,customer.income,customer.result,customer.st=2);
    
  }

  
  predict(customer:Customer){
    this.predictionService.predict(customer.education,customer.income).subscribe(
      res =>{
        console.log(res);
        if (res > 0.5){
          
          customer.result ='will pay';
          customer.st=1;
        } else{
          customer.result = 'will default';
          customer.st=1;
        }
        console.log(customer.result)
      }
    );
  }

    // predict(customer:Customer){
    //   this.predictionService.predict(customer.education,customer.income).subscribe(
    //     res => {
    //       console.log(res);
    //     }
    //   )
    // }

  constructor(public AuthService:AuthService, private CustomersService:CustomersService, private predictionService:PredictionService) { }

  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.customers$ = this.CustomersService.getCusromers(this.userId);
      }
    )

  }

}
