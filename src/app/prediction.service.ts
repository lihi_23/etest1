import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = "https://3w3qka1s2e.execute-api.us-east-1.amazonaws.com/beta";

  predict(education:number, income:number):Observable<any>{
    let json = {
      "data":{
        "education":education,
        "income":income
      }
    }
    console.log(json);
    let body = JSON.stringify(json);
    console.log("in predict");
      return this.http.post<any>(this.url, body).pipe(
        map(res => {
          console.log(res); 
          console.log(res.body);
          return res.body; 
        })
      )
  }

  

  constructor(private http: HttpClient) { }
}
