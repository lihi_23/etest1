import { User } from './interfaces/user';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User|null>


  signUp(email:string, password:string){
    
    return this.afAuth.createUserWithEmailAndPassword(email,password);

  }


  login(email:string, password:string){
    return this.afAuth
         .signInWithEmailAndPassword(email,password);
 
   }

  logout(){
    this.afAuth.signOut();
  }
  getUser():Observable<User | null> {
    return this.user;
    }



  constructor(private afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
   }
}
