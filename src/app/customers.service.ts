import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class CustomersService {





  customerCollection:AngularFirestoreCollection;

  public getCusromers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`);
    return this.customerCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }
  
  public deleteCustomer(userId:string , id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }

  updateCustomer(userId:string,id:string,name:string,education:number,income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        income:income
      }
    )
  }

  save(userId:string,id:string,name:string,education:number,income:number,result:string,st=2){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        income:income,
        result:result,
        st:st
      }

    )
  }
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  
  addCustomer(userId:string,name:string,education:number,income:number,result:string,st:number){
    const customer = {name:name, education:education,income:income, result:result,st:st}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }



  constructor(private db:AngularFirestore) { }
}
