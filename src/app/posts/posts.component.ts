import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { PostsService } from '../posts.service';
import { Comment } from '../interfaces/comment';
import { CommentsService } from '../comments.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  userId:string;
  message:string;
  postId:string;

  posts$:Observable<Post>
  comments$:Observable<Comment>

  // save(post:Post){
  //   this.postsService.addPost(this.userId,post.id,post.title,post.body);
    
  // }

  add(id,title,body){
    this.postsService.addPost(this.userId,id,title,body);
    this.postId  = id;
    this.message = "Saved for later viewing";
    

  }

  constructor(private postsService:PostsService, private commentService:CommentsService,private authService:AuthService) { }

  ngOnInit(): void {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.posts$ = this.postsService.getPosts();
        this.comments$ = this.commentService.getComments();
      }
    )
  }

}
