import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from './interfaces/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  private URL1 = "http://jsonplaceholder.typicode.com/comments";

  constructor(private http: HttpClient) { }

  getComments():Observable<Comment>{
    return this.http.get<Comment>(this.URL1); 
  }

}
