// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig : {
    apiKey: "AIzaSyCZJkv9dgqE6LUCyVu9YxAgbt3abjrybnA",
    authDomain: "etest-3f9c7.firebaseapp.com",
    databaseURL: "https://etest.firebaseio.com",
    projectId: "etest-3f9c7",
    storageBucket: "etest-3f9c7.appspot.com",
    messagingSenderId: "315193056777",
    appId: "1:315193056777:web:5f952ecb8ec0461ee922d8"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
